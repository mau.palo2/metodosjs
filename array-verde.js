//--------Array.fill()--------//
console.log("--------INICIA Array.fill()--------");
let miArregloFill = [0, 0, 0, 0];

//Llenará todo el arreglo con unos
miArregloFill.fill(1)
console.log(miArregloFill);

// Llenará el arreglo con a desde la posición 2 hasta la 4
miArregloFill.fill('a', 2, 4);
console.log(miArregloFill);

//LLenará con 5 desde la posición 1
miArregloFill.fill(5, 1);




//--------Array.filter()--------//
console.log("--------INICIA Array.filter()--------");
//Ejemplo con letras
let miArregloFilter = ["Pepe", "Luis","Filomeno","Ana","Tiburcio"];

let nombresLargos = miArregloFilter.filter(palabra => palabra.length > 6);
console.log(nombresLargos);

//Ejemplo con numeros
miArregloFilter = [1,2,3,4,5];

let numerosPares= miArregloFilter.filter(numP=>numP % 2 == 0);
console.log(numerosPares);




//--------Array.find()--------//
console.log("--------INICIA Array.find()--------");
let miArregloFind = [5, 12, 88, 130, 44];
console.log(miArregloFind);
let numMayorQue10 = miArregloFind.find(num => num > 10);

console.log(numMayorQue10);




//--------Array.findIndex()--------//
console.log("--------INICIA Array.findIndex()--------");
console.log(miArregloFind);
numMayorQue10 = miArregloFind.findIndex(num => num > 10);

console.log(numMayorQue10);



//--------Array.flat()--------//
console.log("--------INICIA Array.flat()--------");
let miArregloFlat = [1, 2, [3, 4]];
console.log(miArregloFlat.flat()); 

miArregloFlat = [1,2, , ,3,4];
console.log(miArregloFlat);
console.log(miArregloFlat.flat()); 



//--------Array.flatMap()--------//
console.log("--------INICIA Array.flatMap()--------");
miArregloFlatMap=[1,2, ,3,4]
console.log(miArregloFlatMap.flatMap(x => [x * 2])); 