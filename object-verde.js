//--------Object.entries()--------//
console.log("--------INICIA Object.entries()--------");
let objetoEntries = {
    a: 'algo',
    b: 42
  };
  
  for (let [key, value] of Object.entries(objetoEntries)) {
    console.log(`${key}: ${value}`);
  }



//--------Object.freeze()--------//
console.log("--------INICIA Object.freeze()--------");
let objetoFreeze = {
    prop: 42
  };
  
  Object.freeze(objetoFreeze);
  
objetoFreeze.prop = 33;
//Aquí debería haber un error
  
  console.log(objetoFreeze.prop);


//--------Object.fromEntries()--------//
console.log("--------INICIA Object.fromEntries()--------");
let entries = new Map([
    ['prop1', 'texto'],
    ['prop2', 42]
  ]);

let entries2 = new Array(['prop3',true])

let objetofromEntries = Object.fromEntries(entries);
console.log(objetofromEntries);

objetofromEntries = Object.fromEntries(entries2);
console.log(objetofromEntries);



//--------Object.getOwnPropertyDescriptor()--------//
console.log("--------INICIA Object.getOwnPropertyDescriptor()--------");

let objetogOPD = {
    property1: 42,
  };
  
  let descriptor1 = Object.getOwnPropertyDescriptor(objetogOPD, 'property1');
  
  console.log(descriptor1.configurable);  
  console.log(descriptor1.value);

let objetogOPD2={};
Object.defineProperty(objetogOPD2, 'incambiable',{
    value:0,
    writable: false
})
let descriptor2= Object.getOwnPropertyDescriptor(objetogOPD2,'incambiable');
console.log(descriptor2.writable);



//--------Object.getOwnPropertyDescriptors()--------//
console.log("--------INICIA Object.getOwnPropertyDescriptors()--------");
let descripciones= Object.getOwnPropertyDescriptors(objetogOPD)
console.log(descripciones.property1);