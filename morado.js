//Array.Methods
/*
//keys() devuelve un nuevo objeto Array Iterator que contiene las claves de índice con las que acceder a cada elemento en el array.
const array = ['a', 'b', 'c'];
const iterator = array.keys();
for (const key of iterator) {  //for of ejecuta un bloque de código para cada elemento de un objeto iterable.
  console.log(key);
} 
// expected output: 0
// expected output: 1
// expected output: 2
*/

/*
//lastIndexOf() devuelve el último índice en el que un cierto elemento puede encontrarse en el arreglo, ó -1 si el elemento no se encontrara.
const colores = ['Rojo', 'Verde', 'Azul', 'Rojo', 'Azul'];
console.log(colores.lastIndexOf('Rojo'));
// expected output: 3
console.log(colores.lastIndexOf('Azul'));
// expected output: 4
console.log(colores.lastIndexOf('Morado'));
// expected output: -1
*/

/*
//map() toma un array y crea uno nuevo con los resultados de la función indicada aplicados a cada uno de sus elementos.
var numeros = [1, 5, 10];
var dobles  = numeros.map(function(num) {
  return num * 2;
});
console.log(numeros); // numeros sigue siendo [1, 5, 10]
console.log(dobles); // dobles es ahora [2, 10, 20]
*/

/*
//of() crea una nueva instancia Array con un número variable de elementos pasados como argumento, independientemente del número o del tipo. La diferencia entre Array.of() y el constructor Array reside en como maneja los parámetros de tipo entero: Array.of(7) crea un array con un solo elemento, 7, mientras que Array(7) crea un array de 7 ranuras vacias.
val = Array(1, 2, 3);     // [1, 2, 3]
val2 = Array.of(1, 2, 3); // [1, 2, 3]
val3 = Array.of(7);       // [7] 
val4 = Array(7);          // [ , , , , , , ] 
console.log(val);
console.log(val2);
console.log(val3);
console.log(val4);
*/

/*
//pop() elimina el último elemento de un array y lo devuelve su valor al método que lo llamó. Este método cambia la longitud del array.
const colores = ['Rojo', 'Verde', 'Azul', 'Morado', 'Negro'];
console.log(colores.pop());
// expected output: "Negro"
console.log(colores);
// expected output: Array ["Rojo", "Verde", "Azul", "Morado"]
colores.pop();
console.log(colores);
// expected output: Array ["Rojo", "Verde", "Azul"]
*/

/*
//push() añade uno o más elementos al final de un array y devuelve la nueva longitud del array.
const colores = ['Rojo', 'Verde', 'Azul'];
console.log(colores);
colores.push('Morado');
console.log(colores);
// expected output: Array ["Rojo", "Verde", "Azul", "Morado"]
colores.push('Negro', 'Blanco');
console.log(colores);
// expected output: Array ["Rojo", "Verde", "Azul", "Morado", "Negro", "Blanco"]
*/

//Object.Methods
/*
//is() determina si dos valores son el mismo por medio de una comparacion estricta ===.
const num = Object.is(25, 25);                // true
const string = Object.is('foo', 'foo');       // true
const num2 = Object.is(7, '7');               // false
console.log(num);
console.log(string);
console.log(num2);
*/

/*
//isExtensible() determina si un objeto es extendible.
const objeto = {};
console.log(Object.isExtensible(objeto));
// expected output: true
Object.preventExtensions(objeto);
console.log(Object.isExtensible(objeto));
// expected output: false
*/

/*
//isFrozen() determina si un objeto esta congelado.
const objeto = {
  property1: 42
};
console.log(Object.isFrozen(objeto));
// expected output: false
Object.freeze(objeto); //no extendible, no configurable
console.log(Object.isFrozen(objeto));
// expected output: true
*/