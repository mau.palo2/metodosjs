// Array.reduce()
console.log("Array.Reduce \n");

// a = 1; c = -1 => a = 0
// a = 0; c = 2 => a = 2
// a = 2; c = 3 => a = 5


let numeros = [1, -1, 2, 3];

let sum = numeros.reduce((accumulator, currentvalue) =>{
   return accumulator + currentvalue;
},5)
 
console.log(sum);


// Array.reduceRight()
console.log("Array.ReduceRight \n");

let nombre = [' angel', ' juan', ' lozano', ' guajardo'];
let nom = nombre.reduceRight((accumulator, currentvalue) =>{

   return accumulator + currentvalue;

}, " tu nombre de usario es: ")
console.log(nom);

// Array.reverse()
console.log("Array.Reverse \n");


let Normal = ['ssssssss', 'x', 'aaaaaaaaaaaaaa'];
console.log('Normal:', Normal );


let Reversa = Normal.reverse();
console.log('O Anoramal:', Reversa);


// Array.shift()
console.log("Array.Shift \n");

let Casa = ['Baño', 'Cocina', 'Patio', 'Jardin'];
// Antes de eliminar
console.log('Primera casa: ' + Casa);
//Variable eliminada
let SinBaño = Casa.shift();
//Despues de eliminar
console.log('Baño eliminado: ' + Casa);

console.log('Elemento: ' + SinBaño);



// Array.slice()
console.log("Array.Slice \n");


let nombres = ['Pedro', 'Miguel', 'Ana', 'Vanesa'];
let masculinos = nombres.slice(0,2 );
console.log(masculinos);
let femen = nombres.slice(2,4);
console.log(femen);



// Array.some()
console.log("Array.Some \n");

let numeros2 = [10, 5 , 10, 2];

  function Mayorde15 (n){
   return n > 15;
  }

console.log(numeros2.some(Mayorde15));